theory CDCL_Two_Watched_Literals_List_Watched_Init_Code
imports CDCL_Two_Watched_Literals_List_Watched_Code
begin

type_synonym 'v twl_st_wl' =
  "('v, nat) ann_lits \<times> 'v clause_l list \<times> nat \<times>
    'v cconflict \<times> 'v clauses \<times> 'v clauses \<times> 'v lit_queue_wl"

type_synonym twl_st_wll' =
  "nat_trail \<times> clauses_wl \<times> nat \<times> nat array_list option \<times> unit_lits_wl \<times> unit_lits_wl \<times>
    lit_queue_l"

definition init_dt_step_wl :: \<open>nat list \<Rightarrow> nat clause_l \<Rightarrow> nat twl_st_wl \<Rightarrow> (nat twl_st_wl) nres\<close> where
  \<open>init_dt_step_wl N\<^sub>1 C S = do {
     let (M, N, U, D, NP, UP, Q, WS) = S in
     case D of
      None \<Rightarrow>
      if length C = 1
      then do {
        ASSERT (no_dup M);
        ASSERT (C \<noteq> []);
        let L = hd C;
        let val_L = valued M L;
        if val_L = None
        then do {RETURN (Propagated L 0 # M, N, U, None, add_mset {#L#} NP, UP, add_mset (-L) Q, WS)}
        else
          if val_L = Some True
          then do {RETURN (M, N, U, None, add_mset {#L#} NP, UP, Q, WS)}
          else do {RETURN (M, N, U, Some (mset C), add_mset {#L#} NP, UP, {#}, WS)}
        }
      else do {
        ASSERT(C \<noteq> []);
        ASSERT(tl C \<noteq> []);
        ASSERT(hd C \<in> snd ` twl_array_code.D\<^sub>0 N\<^sub>1);
        ASSERT(hd (tl C) \<in> snd ` twl_array_code.D\<^sub>0 N\<^sub>1);
        let U = length N;
        let WS = WS((hd C) := WS (hd C) @ [U]);
        let WS = WS((hd (tl C)) := WS (hd (tl C)) @ [U]);
        RETURN (M, N @ [op_array_of_list C], U, None, NP, UP, Q, WS)}
    | Some D \<Rightarrow>
      if length C = 1
      then do {
        ASSERT (C \<noteq> []);
        let L = hd C;
        RETURN (M, N, U, Some D, add_mset {#L#} NP, UP, {#}, WS)}
      else do {
        ASSERT(C \<noteq> []);
        ASSERT(tl C \<noteq> []);
        ASSERT(hd C \<in> snd ` twl_array_code.D\<^sub>0 N\<^sub>1);
        ASSERT(hd (tl C) \<in> snd ` twl_array_code.D\<^sub>0 N\<^sub>1);
        let U = length N;
        let WS = WS(hd C := WS (hd C) @ [U]);
        let WS = WS((hd (tl C)) := WS (hd (tl C)) @ [U]);
        RETURN (M, N @ [op_array_of_list C], U, Some D, NP, UP, {#}, WS)}
  }\<close>
definition arl_of_list_raa :: "'a::heap list \<Rightarrow> ('a array_list) Heap" where
  \<open>arl_of_list_raa xs = do {
    ys \<leftarrow> Array.of_list xs;
    arl_of_array_raa ys }\<close>

lemma arl_of_list_raa_mset[sepref_fr_rules]:
  \<open>(arl_of_list_raa, RETURN o mset) \<in> [\<lambda>C. C \<noteq> []]\<^sub>a(list_assn nat_lit_assn)\<^sup>d \<rightarrow> conflict_assn\<close>
proof -
  define R where \<open>R = nat_lit_rel\<close>
  have [simp]: \<open>x \<noteq> [] \<Longrightarrow> pure (\<langle>R\<rangle>list_rel) x [] = false\<close> for x
    by (auto simp: list_rel_def pure_def)
  show ?thesis
    unfolding R_def[symmetric]
    apply sepref_to_hoare
    unfolding pure_def[symmetric]
    apply (sep_auto simp: arl_of_list_raa_def arl_of_array_raa_def hr_comp_def
        list_mset_rel_def br_def list_assn_pure_conv arl_assn_def is_array_list_def)
    apply (sep_auto simp: pure_def)
    done
qed

declare twl_array_code.append_el_aa_hnr[sepref_fr_rules]

sepref_definition init_dt_step_wl_code
  is \<open>uncurry (init_dt_step_wl N\<^sub>1)\<close>
  :: \<open>(list_assn nat_lit_assn)\<^sup>d *\<^sub>a (twl_array_code.twl_st_l_assn N\<^sub>1)\<^sup>d \<rightarrow>\<^sub>a
       (twl_array_code.twl_st_l_assn N\<^sub>1)\<close>
  unfolding init_dt_step_wl_def twl_array_code.twl_st_l_assn_def lms_fold_custom_empty
      unfolding watched_app_def[symmetric]
  unfolding nth_rll_def[symmetric] find_unwatched'_find_unwatched[symmetric]
  unfolding lms_fold_custom_empty  swap_ll_def[symmetric]
  unfolding twl_array_code.append_update_def[of, symmetric]
  supply [[goals_limit = 1]]
  by sepref

definition extract_lits_cls :: \<open>'a clause_l \<Rightarrow> 'a literal list \<Rightarrow> 'a literal list\<close> where
  \<open>extract_lits_cls C N\<^sub>0 = fold (\<lambda>L N\<^sub>0. if atm_of L \<in> set (map atm_of N\<^sub>0) then N\<^sub>0 else L # N\<^sub>0) C N\<^sub>0\<close>

definition extract_lits_clss:: \<open>'a clauses_l \<Rightarrow> 'a literal list \<Rightarrow> 'a literal list\<close>  where
  \<open>extract_lits_clss N N\<^sub>0 = fold extract_lits_cls N N\<^sub>0\<close>

declare atm_of_hnr[sepref_fr_rules]


definition find_first_eq_map :: "('b \<Rightarrow> 'a) \<Rightarrow> 'a \<Rightarrow> 'b list \<Rightarrow> nat nres" where
  \<open>find_first_eq_map f x xs = WHILE\<^sub>T\<^bsup>\<lambda>i. i \<le> length xs\<^esup>
       (\<lambda>i. i < length xs \<and> f (xs!i) \<noteq> x)
       (\<lambda>i. RETURN (i+1))
       0\<close>

lemma find_first_eq_map_index:
  shows \<open>find_first_eq_map f x xs \<le> \<Down> nat_rel (RETURN (index (map f xs) x))\<close>
proof -
  have H:
    \<open>WHILE\<^sub>T\<^bsup>\<lambda>i. i \<le> length xs\<^esup>
       (\<lambda>i. i < length xs \<and> f (xs!i) \<noteq> x)
       (\<lambda>i. RETURN (i+1))
       k
     \<le> \<Down> nat_rel
       (RETURN (k + index (sublist (map f xs) {k..<length xs}) x))\<close>
    if \<open>k < length xs\<close> for k
    using that
  proof (cases xs)
    case Nil
    then show ?thesis using that by simp
  next
    case xs: (Cons a xs')
    have index_first: \<open>index (sublist (a # xs') {n..<Suc (length xs')}) ((a # xs') ! n) = 0\<close>
      if \<open>n < length xs'\<close> for n
      using that by (metis index_Cons length_Cons less_SucI sublist_upt_Suc)
    have [simp]: "sublist (f a # map f xs') {n..<Suc (length xs')} =
    (f a # map f xs') ! n # sublist (f a # map f xs') {Suc n..<Suc (length xs')}"
      if a2: "n < length xs'" for n -- \<open>auto is not able to derive it automatically
      because of @{thm length_Cons}\<close>
      using a2
      apply (subst length_Cons[of a, symmetric])+
      apply (subst length_map[of f \<open>a # xs'\<close>, symmetric])+
      by (metis length_Cons length_map less_SucI sublist_upt_Suc)
    have [simp]: \<open>(f a # map f xs') ! n = f ((a # xs') ! n)\<close> if \<open>n < length (a#xs')\<close> for n
      unfolding list.map[symmetric]
      by (subst nth_map) (use that in auto)

    have \<open>k < Suc (length xs')\<close>
      using that xs by auto
    then show ?thesis
      unfolding find_first_eq_def less_eq_Suc_le Suc_le_mono xs
      apply (induction rule: inc_induct)
      subgoal by (auto simp: sublist_single_if WHILEIT_unfold )[]
      subgoal by (subst WHILEIT_unfold) (auto simp: sublist_single_if index_first sublist_upt_Suc)
      done
  qed
  have [simp]: \<open>find_first_eq_map f x [] \<le> RETURN 0\<close>
    unfolding find_first_eq_map_def by (auto simp: WHILEIT_unfold)[]
  have [simp]: \<open>sublist (map f xs) {0..<length xs} = map f xs\<close>
    by (simp add: sublist_id_iff)
  show ?thesis
    apply (cases \<open>xs = []\<close>)
     apply (solves simp)
    using H[of 0] unfolding find_first_eq_map_def by simp
qed

sepref_definition find_first_eq_map_atm_of_code
  is \<open>uncurry (find_first_eq_map atm_of)\<close>
  :: \<open>nat_assn\<^sup>k *\<^sub>a (list_assn nat_lit_assn)\<^sup>k \<rightarrow>\<^sub>a nat_assn\<close>
  unfolding find_first_eq_map_def short_circuit_conv
  by sepref
definition index_atm_of where
  \<open>index_atm_of L N\<^sub>0 = index (map atm_of N\<^sub>0) L\<close>

lemma find_first_eq_map_atm_of_code_index_atm_of[sepref_fr_rules]:
  \<open>(uncurry find_first_eq_map_atm_of_code, uncurry (RETURN oo index_atm_of)) \<in>
     nat_assn\<^sup>k *\<^sub>a (list_assn nat_lit_assn)\<^sup>k \<rightarrow>\<^sub>a nat_assn\<close>
proof -
  have 1: \<open>(uncurry (find_first_eq_map atm_of), uncurry (RETURN oo index_atm_of)) \<in>
    Id \<times>\<^sub>f \<langle>Id\<rangle>list_rel \<rightarrow>\<^sub>f \<langle>nat_rel\<rangle>nres_rel\<close>
    unfolding uncurry_def
    apply (intro nres_relI frefI, rename_tac x y)
    apply (case_tac x, case_tac y, simp)
    by (smt find_first_eq_map_index index_atm_of_def refine_IdD)
  show ?thesis
    using find_first_eq_map_atm_of_code.refine[FCOMP 1] .
qed

definition extract_lits_cls' :: \<open>'a clause_l \<Rightarrow> 'a literal list \<Rightarrow> 'a literal list\<close> where
  \<open>extract_lits_cls' C N\<^sub>0 =
     fold (\<lambda>L N\<^sub>0. let i = index (map atm_of N\<^sub>0) (atm_of L) in if i < length N\<^sub>0 then N\<^sub>0 else L # N\<^sub>0) C N\<^sub>0\<close>

thm atm_of_hnr
sepref_definition extract_lits_cls_code
  is \<open>uncurry (RETURN oo extract_lits_cls')\<close>
  :: \<open>(list_assn nat_lit_assn)\<^sup>d *\<^sub>a (list_assn nat_lit_assn)\<^sup>d \<rightarrow>\<^sub>a (list_assn nat_lit_assn)\<close>
  unfolding extract_lits_cls'_def twl_array_code.twl_st_l_assn_def lms_fold_custom_empty
      unfolding watched_app_def[symmetric]
  unfolding nth_rll_def[symmetric] find_unwatched'_find_unwatched[symmetric]
  unfolding lms_fold_custom_empty  swap_ll_def[symmetric]
  unfolding twl_array_code.append_update_def[of, symmetric]
    index_atm_of_def[symmetric]
  supply [[goals_limit = 1]]
  by sepref

declare extract_lits_cls_code.refine[sepref_fr_rules]

lemma extract_lits_cls'_extract_lits_cls: \<open>extract_lits_cls' = extract_lits_cls\<close>
proof -
  have [simp]: \<open>(\<lambda>L N\<^sub>0. if index (map atm_of N\<^sub>0) (atm_of L) < length N\<^sub>0 then N\<^sub>0 else L # N\<^sub>0) =
      (\<lambda>L N\<^sub>0. if atm_of L \<in> atm_of ` set N\<^sub>0 then N\<^sub>0 else L # N\<^sub>0)\<close>
    by (intro ext) (auto simp: extract_lits_cls'_def extract_lits_cls_def)
  then show ?thesis
    by (intro ext) (auto simp: extract_lits_cls'_def extract_lits_cls_def)
qed
sepref_definition extract_lits_clss_code
  is \<open>uncurry (RETURN oo extract_lits_clss)\<close>
  :: \<open>(list_assn (list_assn nat_lit_assn))\<^sup>d *\<^sub>a (list_assn nat_lit_assn)\<^sup>d \<rightarrow>\<^sub>a (list_assn nat_lit_assn)\<close>
  unfolding extract_lits_clss_def extract_lits_cls'_extract_lits_cls[symmetric]
  by sepref

declare extract_lits_clss_code.refine[sepref_fr_rules]

lemma lits_of_atms_of_mm_empty[simp]: \<open>lits_of_atms_of_mm {#} = {#}\<close>
  by (auto simp: lits_of_atms_of_mm_def)

lemma lits_of_atms_of_m_empty[simp]: \<open>lits_of_atms_of_m {#} = {#}\<close>
  by (auto simp: lits_of_atms_of_m_def)

lemma extract_lits_cls_Cons:
  \<open>extract_lits_cls (L # C) N\<^sub>0 = extract_lits_cls C (if atm_of L \<in> atms_of (mset N\<^sub>0) then N\<^sub>0 else L # N\<^sub>0)\<close>
  unfolding extract_lits_cls_def fold.simps by simp

lemma extract_lits_cls_Nil[simp]:
  \<open>extract_lits_cls [] N\<^sub>0 = N\<^sub>0\<close>
  unfolding extract_lits_cls_def fold.simps by simp

lemma extract_lits_clss_Cons[simp]:
  \<open>extract_lits_clss (C # Cs) N = extract_lits_clss Cs (extract_lits_cls C N)\<close>
  by (simp add: extract_lits_clss_def)

lemma lits_of_atms_of_m_extract_lits_cls: \<open>set_mset (lits_of_atms_of_m (mset (extract_lits_cls C N\<^sub>0))) =
   set_mset (lits_of_atms_of_m (mset C) + lits_of_atms_of_m (mset N\<^sub>0))\<close>
  apply (induction C arbitrary: N\<^sub>0)
  subgoal by simp
  subgoal by (simp add: extract_lits_cls_Cons lits_of_atms_of_m_add_mset
        in_lits_of_atms_of_m_ain_atms_of_iff insert_absorb)
  done

lemma is_N\<^sub>1_extract_lits_clss: \<open>twl_array_code.is_N\<^sub>1 (map nat_of_lit (extract_lits_clss N N\<^sub>0))
  (lits_of_atms_of_mm (mset `# mset N) + lits_of_atms_of_m (mset N\<^sub>0))\<close>
proof -
  have is_N\<^sub>1_add: \<open>twl_array_code.is_N\<^sub>1 N\<^sub>0 (A + B) \<longleftrightarrow> set_mset A \<subseteq> set_mset (twl_array_code.N\<^sub>1 N\<^sub>0)\<close>
    if \<open>twl_array_code.is_N\<^sub>1 N\<^sub>0 B\<close> for A B N\<^sub>0
    using that unfolding twl_array_code.is_N\<^sub>1_def by auto
  show ?thesis
    apply (induction N arbitrary: N\<^sub>0)
    subgoal by (auto simp: extract_lits_cls_def extract_lits_clss_def  twl_array_code.is_N\<^sub>1_def
          twl_array_code.N\<^sub>1_def in_lits_of_atms_of_m_ain_atms_of_iff twl_array_code.N\<^sub>0''_def
          twl_array_code.N\<^sub>0'_def atm_of_eq_atm_of
          simp del: nat_of_lit.simps literal_of_nat.simps)
    subgoal premises H for C Cs N\<^sub>0
      using H[of \<open>extract_lits_cls C N\<^sub>0\<close>, unfolded twl_array_code.is_N\<^sub>1_def, symmetric]
      by (simp add: lits_of_atms_of_mm_add_mset twl_array_code.is_N\<^sub>1_def
          lits_of_atms_of_m_extract_lits_cls ac_simps)
    done
qed

fun correct_watching_init :: \<open>nat literal multiset \<Rightarrow> nat twl_st_wl \<Rightarrow> bool\<close> where
  \<open>correct_watching_init N\<^sub>1 (M, N, U, D, NP, UP, Q, W) \<longleftrightarrow>
    (\<forall>L \<in># lits_of_atms_of_m N\<^sub>1. mset (W L) = clause_to_update L (M, N, U, D, NP, UP, {#}, {#}))\<close>


lemma clause_to_update_append: \<open>N \<noteq> [] \<Longrightarrow> clause_to_update La (M, N @ [C], U, D, NP, UP, WS, Q) =
     clause_to_update La (M, N, U, D, NP, UP, WS, Q) +
    (if La \<in> set (watched_l C) then {#length N#} else {#})\<close>
  unfolding clause_to_update_def get_clauses_l.simps
  apply (auto simp: clause_to_update_def nth_append)
  by meson

lemma literal_of_nat_literal_of_nat_eq[iff]: \<open>literal_of_nat x = literal_of_nat xa \<longleftrightarrow> x = xa\<close>
  by auto presburger+

definition HH :: \<open>nat literal multiset \<Rightarrow> (nat twl_st_wl \<times> nat twl_st_l) set\<close> where
  \<open>HH N\<^sub>1 = {((M', N', U', D', NP', UP', Q', WS'), (M, N, U, D, NP, UP, WS, Q)).
               M = M' \<and> N = N' \<and> U = U' \<and> D = D' \<and> NP = NP' \<and> UP = UP' \<and> Q = Q' \<and> WS = {#} \<and>
               (* U = length N - 1 \<and> *) UP = {#} \<and> N \<noteq> [] \<and>
               correct_watching_init N\<^sub>1 (M', N', U', D', NP', UP', Q', WS') \<and>
               set_mset (lits_of_atms_of_mm (mset `# mset (tl N) + NP)) \<subseteq> set_mset N\<^sub>1 \<and>
               (\<forall>L \<in> lits_of_l M. {#L#} \<in># NP) \<and>
               (\<forall>L \<in> set M. \<exists>K. L = Propagated K 0)}\<close>

thm twl_array_code.literals_are_in_N\<^sub>0_def

lemma literals_are_in_N\<^sub>0_add_mset:
  \<open>twl_array_code.literals_are_in_N\<^sub>0 N\<^sub>0 (add_mset L M) \<longleftrightarrow>
   twl_array_code.literals_are_in_N\<^sub>0 N\<^sub>0 M \<and> (L \<in> literal_of_nat ` set N\<^sub>0 \<or> -L \<in> literal_of_nat ` set N\<^sub>0)\<close>
  by (auto simp: twl_array_code.N\<^sub>1_def twl_array_code.N\<^sub>0''_def twl_array_code.N\<^sub>0'_def
      twl_array_code.literals_are_in_N\<^sub>0_def image_image lits_of_atms_of_m_add_mset uminus_lit_swap
        simp del: literal_of_nat.simps)

lemma init_dt_step_wl_init_dt_step_l:
  fixes N\<^sub>0 :: \<open>nat list\<close>
  defines \<open>N\<^sub>1 \<equiv> mset (map literal_of_nat N\<^sub>0) + mset (map (uminus o literal_of_nat) N\<^sub>0)\<close>
  assumes
    \<open>(S', S) \<in> HH N\<^sub>1\<close> and
    \<open>twl_array_code.literals_are_in_N\<^sub>0 N\<^sub>0 (mset C)\<close> and
    \<open>distinct C\<close>
  shows \<open>init_dt_step_wl N\<^sub>0 C S' \<le> \<Down> (HH N\<^sub>1) (init_dt_step_l C S)\<close>
proof -
  have [simp]: \<open>N\<^sub>1 = twl_array_code.N\<^sub>1 N\<^sub>0\<close>
    by (auto simp: twl_array_code.N\<^sub>1_def N\<^sub>1_def twl_array_code.N\<^sub>0''_def twl_array_code.N\<^sub>0'_def
        simp del: literal_of_nat.simps)
  have [iff]: \<open>- L \<in># twl_array_code.N\<^sub>1 N\<^sub>0 \<longleftrightarrow> L \<in># twl_array_code.N\<^sub>1 N\<^sub>0\<close> for L
    by (auto simp: twl_array_code.N\<^sub>1_def N\<^sub>1_def twl_array_code.N\<^sub>0''_def twl_array_code.N\<^sub>0'_def
        uminus_lit_swap simp del: literal_of_nat.simps)
  have [simp]: \<open>clause_to_update L (M, N, U, D, NP, UP, WS, Q) =
       clause_to_update L (M', N', U', D', NP', UP', WS', Q')\<close>
    if \<open>N = N'\<close>
    for M N U D NP UP WS Q M' N' U' D' NP' UP' WS' Q' and L :: \<open>nat literal\<close>
    by (auto simp: clause_to_update_def that)
  show ?thesis
    supply literal_of_nat.simps[simp del]
    using assms(2-)
    unfolding init_dt_step_wl_def init_dt_step_l_def
    apply refine_rcg
    subgoal by (auto simp: HH_def)
    subgoal by fast
    subgoal by (auto simp: HH_def)
    subgoal by (auto simp: HH_def)
    subgoal by (cases C) (auto simp: HH_def correct_watching.simps clause_to_update_def
          lits_of_atms_of_mm_add_mset lits_of_atms_of_m_add_mset twl_array_code.N\<^sub>1_def
          twl_array_code.N\<^sub>0''_def twl_array_code.N\<^sub>0'_def
          twl_array_code.literals_are_in_N\<^sub>0_def clauses_def mset_take_mset_drop_mset')
    subgoal by (auto simp: HH_def)
    subgoal by (cases C) (clarsimp_all simp: HH_def correct_watching.simps clause_to_update_def
          lits_of_atms_of_mm_add_mset lits_of_atms_of_m_add_mset twl_array_code.N\<^sub>1_def
          twl_array_code.N\<^sub>0''_def twl_array_code.N\<^sub>0'_def
          twl_array_code.literals_are_in_N\<^sub>0_def clauses_def mset_take_mset_drop_mset')
    subgoal by (cases C) (clarsimp_all simp: HH_def correct_watching.simps clause_to_update_def
          lits_of_atms_of_mm_add_mset lits_of_atms_of_m_add_mset twl_array_code.N\<^sub>1_def
          twl_array_code.N\<^sub>0''_def twl_array_code.N\<^sub>0'_def
          twl_array_code.literals_are_in_N\<^sub>0_def clauses_def mset_take_mset_drop_mset')
    subgoal by (cases C) (clarsimp_all simp: HH_def
          lits_of_atms_of_mm_add_mset lits_of_atms_of_m_add_mset image_image
          twl_array_code.literals_are_in_N\<^sub>0_def clauses_def mset_take_mset_drop_mset')
    subgoal by (cases C; cases \<open>tl C\<close>) (clarsimp_all simp: HH_def
          lits_of_atms_of_mm_add_mset lits_of_atms_of_m_add_mset image_image
          twl_array_code.literals_are_in_N\<^sub>0_def clauses_def mset_take_mset_drop_mset')
    subgoal by (cases C; cases \<open>tl C\<close>) (auto simp: HH_def Let_def clause_to_update_append
          clauses_def mset_take_mset_drop_mset' lits_of_atms_of_m_add_mset
          lits_of_atms_of_mm_add_mset literals_are_in_N\<^sub>0_add_mset
          twl_array_code.literals_are_in_N\<^sub>0_def)
    subgoal by fast
    subgoal by (cases C) (clarsimp_all simp: HH_def correct_watching.simps clause_to_update_def
          lits_of_atms_of_mm_add_mset lits_of_atms_of_m_add_mset twl_array_code.N\<^sub>1_def
          twl_array_code.N\<^sub>0''_def twl_array_code.N\<^sub>0'_def
          twl_array_code.literals_are_in_N\<^sub>0_def clauses_def mset_take_mset_drop_mset')
    subgoal by (cases C; cases \<open>tl C\<close>) (clarsimp_all simp: HH_def
          lits_of_atms_of_mm_add_mset lits_of_atms_of_m_add_mset image_image
          twl_array_code.literals_are_in_N\<^sub>0_def clauses_def mset_take_mset_drop_mset')
    subgoal by (cases C; cases \<open>tl C\<close>) (auto simp: HH_def Let_def clause_to_update_append
          clauses_def mset_take_mset_drop_mset' image_image lits_of_atms_of_m_add_mset
          twl_array_code.literals_are_in_N\<^sub>0_def)
    subgoal by (cases C; cases \<open>tl C\<close>) (auto simp: HH_def Let_def clause_to_update_append
          clauses_def mset_take_mset_drop_mset' lits_of_atms_of_m_add_mset
          lits_of_atms_of_mm_add_mset literals_are_in_N\<^sub>0_add_mset
          twl_array_code.literals_are_in_N\<^sub>0_def)
    done
qed

definition init_dt_wl :: \<open>nat list \<Rightarrow> nat clauses_l \<Rightarrow> nat twl_st_wl \<Rightarrow> (nat twl_st_wl) nres\<close> where
  \<open>init_dt_wl N\<^sub>1 CS S = nfoldli CS (\<lambda>_. True) (init_dt_step_wl N\<^sub>1) S\<close>

lemma init_dt_wl_init_dt_l:
  fixes N\<^sub>0 :: \<open>nat list\<close>
  defines \<open>N\<^sub>1 \<equiv> mset (map literal_of_nat N\<^sub>0) + mset (map (uminus o literal_of_nat) N\<^sub>0)\<close>
  assumes
    S'S: \<open>(S', S) \<in> HH N\<^sub>1\<close> and
    \<open>\<forall>C\<in>set CS. twl_array_code.literals_are_in_N\<^sub>0 N\<^sub>0 (mset C)\<close> and
    \<open>\<forall>C\<in>set CS. distinct C\<close>
  shows \<open>init_dt_wl N\<^sub>0 CS S' \<le> \<Down> (HH N\<^sub>1) (init_dt_l CS S)\<close>
  using assms(2-)
  supply literal_of_nat.simps[simp del]
  apply (induction CS arbitrary: S S')
  subgoal using S'S by (simp add: init_dt_wl_def init_dt_l_def)
  subgoal premises p for a CS S S'
    using p(2-)
    unfolding init_dt_wl_def init_dt_l_def nfoldli_simps(2) if_True apply -
    apply (rule bind_refine)
     apply (rule init_dt_step_wl_init_dt_step_l[of _ _ N\<^sub>0, unfolded N\<^sub>1_def[symmetric]]; solves \<open>simp\<close>)
    apply (rule p(1)[unfolded init_dt_wl_def init_dt_l_def]; solves \<open>simp\<close>)
    done
  done

abbreviation twl_code_array_literals_are_N\<^sub>0 where
  \<open>twl_code_array_literals_are_N\<^sub>0 N\<^sub>0 S \<equiv>
     twl_array_code.is_N\<^sub>1 N\<^sub>0 (lits_of_atms_of_mm (cdcl\<^sub>W_restart_mset.clauses (convert_to_state (twl_st_of_wl None S))))\<close>

fun get_clauses_wl :: "'v twl_st_wl \<Rightarrow> 'v clauses_l" where
  \<open>get_clauses_wl (M, N, U, D, NP, UP, WS, Q) = N\<close>

fun get_learned_wl :: "'v twl_st_wl \<Rightarrow> nat" where
  \<open>get_learned_wl (M, N, U, D, NP, UP, WS, Q) = U\<close>

lemma lits_of_atms_of_mm_in_lits_of_atms_of_m_in_iff:
  \<open>set_mset (lits_of_atms_of_mm (mset `# mset CS)) \<subseteq> A \<longleftrightarrow>
    (\<forall>C\<in>set CS. set_mset (lits_of_atms_of_m (mset C)) \<subseteq> A)\<close>
  by (auto simp: lits_of_atms_of_mm_def lits_of_atms_of_m_def)

definition get_unit_learned :: "'v twl_st_wl \<Rightarrow> 'v clauses" where
  \<open>get_unit_learned = (\<lambda>(M, N, U, D, NP, UP, Q, W). UP)\<close>

definition get_unit_init_clss :: "'v twl_st_wl \<Rightarrow> 'v clauses" where
  \<open>get_unit_init_clss = (\<lambda>(M, N, U, D, NP, UP, Q, W). NP)\<close>


lemma init_dt_init_dt_l_full:
  fixes S :: \<open>nat twl_st_wl\<close> and CS
  defines \<open>N\<^sub>0 \<equiv> map nat_of_lit (extract_lits_clss CS [])\<close>
  defines \<open>N\<^sub>1 \<equiv> mset (map literal_of_nat N\<^sub>0) + mset (map (uminus \<circ> literal_of_nat) N\<^sub>0)\<close>
  assumes
    dist: \<open>\<forall>C \<in> set CS. distinct C\<close> and
    length: \<open>\<forall>C \<in> set CS. length C \<ge> 1\<close> and
    taut: \<open>\<forall>C \<in> set CS. \<not>tautology (mset C)\<close> and
    struct: \<open>twl_struct_invs (twl_st_of_wl None S)\<close> and
    dec:\<open>\<forall>s\<in>set (get_trail_wl S). \<not>is_decided s\<close> and
    confl: \<open>get_conflict_wl S = None \<longrightarrow> pending_wl S = uminus `# lit_of `# mset (get_trail_wl S)\<close> and
    aff_invs: \<open>additional_WS_invs (st_l_of_wl None S)\<close> and
    learned: \<open>get_learned_wl S = length (get_clauses_wl S) - 1\<close> and
    stgy_invs: \<open>twl_stgy_invs (twl_st_of_wl None S)\<close> and
    watch: \<open>correct_watching_init N\<^sub>1 S\<close> and
    clss: \<open>get_clauses_wl S \<noteq> []\<close> and
    S_N\<^sub>1: \<open>set_mset (lits_of_atms_of_mm (cdcl\<^sub>W_restart_mset.clauses
      (convert_to_state (twl_st_of None (st_l_of_wl None S))))) \<subseteq> set_mset N\<^sub>1\<close> and
    no_learned: \<open>get_unit_learned S = {#}\<close> and
    confl_in_clss: \<open>get_conflict_wl S \<noteq> None \<longrightarrow> the (get_conflict_wl S) \<in># mset `# mset CS\<close> and
    trail_in_NP: \<open>\<forall>L \<in> lits_of_l (get_trail_wl S). {#L#} \<in># get_unit_init_clss S\<close> and
    prop_NP: \<open>\<forall>L \<in> set (get_trail_wl S). \<exists>K. L = Propagated K 0\<close>
  shows
    \<open>init_dt_wl N\<^sub>0 CS S \<le> SPEC(\<lambda>T.
       twl_array_code.is_N\<^sub>1 N\<^sub>0 (lits_of_atms_of_mm (mset `# mset CS +
          cdcl\<^sub>W_restart_mset.clauses (convert_to_state (twl_st_of None (st_l_of_wl None S))))) \<and>
       twl_struct_invs (twl_st_of_wl None T) \<and> twl_stgy_invs (twl_st_of_wl None T) \<and>
       additional_WS_invs (st_l_of_wl None T) \<and>
       correct_watching_init N\<^sub>1 T \<and>
       cdcl\<^sub>W_restart_mset.clauses (convert_to_state (twl_st_of None (st_l_of_wl None T))) =
         mset `# mset CS +
         cdcl\<^sub>W_restart_mset.clauses (convert_to_state (twl_st_of None (st_l_of_wl None S))) \<and>
      get_unit_learned T = {#} \<and>
      get_learned_wl T = length (get_clauses_wl T) - 1 \<and>
      count_decided (get_trail_wl T) = 0 \<and>
      (get_conflict_wl T \<noteq> None \<longrightarrow> the (get_conflict_wl T) \<in># mset `# mset CS) \<and>
      (\<forall>L \<in> lits_of_l (get_trail_wl T). {#L#} \<in># get_unit_init_clss T) \<and>
      (\<forall>L \<in> set (get_trail_wl T). \<exists>K. L = Propagated K 0))\<close>
proof -
  define T where \<open>T = st_l_of_wl None S\<close>
  have N\<^sub>0_N\<^sub>1: \<open>twl_array_code.N\<^sub>1 N\<^sub>0 = N\<^sub>1\<close>
    by (auto simp: twl_array_code.N\<^sub>1_def N\<^sub>1_def twl_array_code.N\<^sub>0''_def twl_array_code.N\<^sub>0'_def
        simp del: nat_of_lit.simps literal_of_nat.simps)
  have w_q: \<open>working_queue_l T = {#}\<close>
    by (cases S) (simp add: T_def)
  have tr_T_S: \<open>get_trail_l T = get_trail_wl S\<close> and p_T_S: \<open>pending_l T = pending_wl S\<close> and
    c_T_S: \<open>get_conflict_l T = get_conflict_wl S\<close> and
    l_T_S: \<open>get_learned_l T = get_learned_wl S\<close>and
    cl_T_S: \<open>get_clauses_l T = get_clauses_wl S\<close>
    by (cases S; simp add: T_def)+
  have
    dist_T: \<open>\<forall>C \<in> set (rev CS). distinct C\<close> and
    length_T: \<open>\<forall>C \<in> set (rev CS). length C \<ge> 1\<close> and
    taut_T: \<open>\<forall>C \<in> set (rev CS). \<not>tautology (mset C)\<close> and
    struct_T: \<open>twl_struct_invs (twl_st_of None T)\<close> and
    stgy_T: \<open>twl_stgy_invs (twl_st_of None T)\<close> and
    w_q_T: \<open>working_queue_l T = {#}\<close> and
    tr_T: \<open>\<forall>s\<in>set (get_trail_l T). \<not> is_decided s\<close> and
    c_T: \<open>get_conflict_l T = None \<longrightarrow> pending_l T = uminus `# lit_of `# mset (get_trail_l T)\<close> and
    add_invs_T: \<open>additional_WS_invs T\<close> and
    le_T: \<open>get_learned_l T = length (get_clauses_l T) - 1\<close> and
    confl_in_clss_T: \<open>get_conflict_l T \<noteq> None \<longrightarrow> the (get_conflict_l T) \<in># mset `# mset (rev CS)\<close>
    by (use assms(3-) in \<open>simp add: T_def[symmetric]  w_q tr_T_S p_T_S c_T_S l_T_S cl_T_S\<close>)+
  note init = init_dt_full[of \<open>rev CS\<close> T, OF dist_T length_T taut_T struct_T w_q_T tr_T c_T
      add_invs_T le_T stgy_T ] init_dt_confl_in_clauses[OF confl_in_clss_T]
  have i: \<open>init_dt_l CS T \<le> \<Down> Id (SPEC(\<lambda>T. twl_struct_invs (twl_st_of None T) \<and> twl_stgy_invs (twl_st_of None T) \<and>
      cdcl\<^sub>W_restart_mset.clauses (convert_to_state (twl_st_of None T)) =
        mset `# mset CS + cdcl\<^sub>W_restart_mset.clauses (convert_to_state (twl_st_of_wl None S)) \<and>
      get_learned_l T = length (get_clauses_l T) - 1 \<and> additional_WS_invs T \<and>
      count_decided (get_trail_l T) = 0 \<and>
      (get_conflict_l T \<noteq> None \<longrightarrow> the (get_conflict_l T) \<in># mset `# mset CS)))
      \<close>
    apply (subst init_dt_init_dt_l[of \<open>rev CS\<close>, unfolded rev_rev_ident, symmetric];
        use assms(3-) in \<open>simp add: T_def[symmetric]  w_q tr_T_S p_T_S c_T_S l_T_S cl_T_S\<close>)
    apply (intro conjI)
    using init apply (simp_all add: count_decided_def)
    done
   have CS_N\<^sub>1: \<open>\<forall>C\<in>set CS. twl_array_code.literals_are_in_N\<^sub>0 N\<^sub>0 (mset C)\<close>
    using is_N\<^sub>1_extract_lits_clss[of CS \<open>[]\<close>] unfolding N\<^sub>1_def N\<^sub>0_def
      twl_array_code.is_N\<^sub>1_def twl_array_code.N\<^sub>1_def twl_array_code.N\<^sub>0''_def
      twl_array_code.N\<^sub>0'_def twl_array_code.literals_are_in_N\<^sub>0_def
    by (simp del: literal_of_nat.simps add: lits_of_atms_of_mm_in_lits_of_atms_of_m_in_iff[symmetric])
  have is_N\<^sub>1: \<open>twl_array_code.is_N\<^sub>1 N\<^sub>0 (lits_of_atms_of_mm (mset `# mset CS))\<close>
    using is_N\<^sub>1_extract_lits_clss[of CS \<open>[]\<close>] unfolding N\<^sub>1_def N\<^sub>0_def
      twl_array_code.is_N\<^sub>1_def twl_array_code.N\<^sub>1_def twl_array_code.N\<^sub>0''_def
      twl_array_code.N\<^sub>0'_def twl_array_code.literals_are_in_N\<^sub>0_def
    by (simp del: literal_of_nat.simps add: lits_of_atms_of_mm_in_lits_of_atms_of_m_in_iff[symmetric])
  have Un_eq_iff_subset: \<open>A \<union> B = A \<longleftrightarrow> B \<subseteq> A\<close> for A B
    by blast
  have [simp]: \<open>twl_array_code.is_N\<^sub>1 N\<^sub>0 (A + lits_of_atms_of_mm B) \<longleftrightarrow>
       set_mset (lits_of_atms_of_mm B) \<subseteq> set_mset N\<^sub>1\<close>
    if \<open>twl_array_code.is_N\<^sub>1 N\<^sub>0 A\<close> for A B
    using that by (simp add: twl_array_code.is_N\<^sub>1_def twl_array_code.literals_are_in_N\<^sub>0_def
        Un_eq_iff_subset N\<^sub>0_N\<^sub>1)
  have CS_N\<^sub>1': \<open>set_mset (lits_of_atms_of_mm (mset `# mset CS)) \<subseteq> set_mset N\<^sub>1\<close>
    using is_N\<^sub>1 unfolding twl_array_code.is_N\<^sub>1_def by (clarsimp simp: HH_def lits_of_atms_of_mm_union N\<^sub>0_N\<^sub>1)
  have \<open>set_mset (lits_of_atms_of_mm (cdcl\<^sub>W_restart_mset.clauses
      (convert_to_state (twl_st_of None (st_l_of_wl None S))))) \<subseteq>
   set_mset N\<^sub>1\<close>
    using S_N\<^sub>1 unfolding twl_array_code.is_N\<^sub>1_def
    by (cases S)(clarsimp simp: mset_take_mset_drop_mset' clauses_def HH_def N\<^sub>0_N\<^sub>1 cl_T_S)
  have [simp]: \<open>mset `# mset (take ag (tl af)) + ai + (mset `# mset (drop (Suc ag) af)) =
     mset `# mset (tl af) + ai\<close> for ag af aj ai
    by (subst (2) append_take_drop_id[symmetric, of \<open>tl af\<close> ag], subst mset_append)
      (auto simp: drop_Suc)
  show ?thesis
    apply (rule order.trans)
     apply (rule conc_trans[OF init_dt_wl_init_dt_l i, of S N\<^sub>0, unfolded N\<^sub>1_def[symmetric]])
    subgoal using clss watch S_N\<^sub>1 no_learned trail_in_NP prop_NP
      by (auto simp: HH_def T_def clauses_def mset_take_mset_drop_mset' get_unit_learned_def
            get_unit_init_clss_def
          simp del: correct_watching_init.simps)
    subgoal using CS_N\<^sub>1 by auto
    subgoal using dist .
    subgoal using is_N\<^sub>1 CS_N\<^sub>1' S_N\<^sub>1 unfolding conc_fun_RES
      by (clarsimp simp: HH_def lits_of_atms_of_mm_union mset_take_mset_drop_mset'
          clauses_def get_unit_learned_def get_unit_init_clss_def)
    done
qed

lemma init_dt_init_dt_l:
  fixes CS
  defines \<open>S \<equiv> ([], [[]], 0, None, {#}, {#}, {#}, \<lambda>_. [])\<close>
  defines \<open>N\<^sub>0 \<equiv> map nat_of_lit (extract_lits_clss CS [])\<close>
  defines \<open>N\<^sub>1 \<equiv> mset (map literal_of_nat N\<^sub>0) + mset (map (uminus \<circ> literal_of_nat) N\<^sub>0)\<close>
  assumes
    dist: \<open>\<forall>C \<in> set CS. distinct C\<close> and
    length: \<open>\<forall>C \<in> set CS. length C \<ge> 1\<close> and
    taut: \<open>\<forall>C \<in> set CS. \<not>tautology (mset C)\<close>
   shows
    \<open>init_dt_wl N\<^sub>0 CS S \<le> SPEC(\<lambda>S.
       twl_array_code.is_N\<^sub>1 N\<^sub>0 (lits_of_atms_of_mm (mset `# mset CS)) \<and>
       twl_struct_invs (twl_st_of_wl None S) \<and> twl_stgy_invs (twl_st_of_wl None S) \<and>
       correct_watching S \<and>
       additional_WS_invs (st_l_of_wl None S) \<and>
       get_unit_learned S = {#} \<and>
       count_decided (get_trail_wl S) = 0 \<and>
       get_learned_wl S = length (get_clauses_wl S) - 1 \<and>
       cdcl\<^sub>W_restart_mset.clauses (convert_to_state (twl_st_of None (st_l_of_wl None S))) =
         mset `# mset CS \<and>
       (get_conflict_wl S \<noteq> None \<longrightarrow> the (get_conflict_wl S) \<in># mset `# mset CS) \<and>
       (\<forall>L\<in>lits_of_l (get_trail_wl S). {#L#} \<in># get_unit_init_clss S) \<and>
       (\<forall>L \<in> set (get_trail_wl S). \<exists>K. L = Propagated K 0))\<close>
proof -
  have clss_empty: \<open>cdcl\<^sub>W_restart_mset.clauses (convert_to_state (twl_st_of None (st_l_of_wl None S))) = {#}\<close>
    by (auto simp: S_def  cdcl\<^sub>W_restart_mset.clauses_def cdcl\<^sub>W_restart_mset_state)
  have
    struct: \<open>twl_struct_invs (twl_st_of_wl None S)\<close> and
    dec:\<open>\<forall>s\<in>set (get_trail_wl S). \<not>is_decided s\<close> and
    confl: \<open>get_conflict_wl S = None \<longrightarrow> pending_wl S = uminus `# lit_of `# mset (get_trail_wl S)\<close> and
    aff_invs: \<open>additional_WS_invs (st_l_of_wl None S)\<close> and
    learned: \<open>get_learned_wl S = length (get_clauses_wl S) - 1\<close> and
    stgy_invs: \<open>twl_stgy_invs (twl_st_of_wl None S)\<close> and
    watch: \<open>correct_watching_init N\<^sub>1 S\<close> and
    clss: \<open>get_clauses_wl S \<noteq> []\<close> and
    S_N\<^sub>1: \<open>set_mset (lits_of_atms_of_mm (cdcl\<^sub>W_restart_mset.clauses
      (convert_to_state (twl_st_of None (st_l_of_wl None S))))) \<subseteq> set_mset N\<^sub>1\<close> and
    no_learned: \<open>(\<lambda>(M, N, U, D, NP, UP, Q, W). UP) S = {#}\<close> and
    learned_nil: \<open>get_unit_learned S = {#}\<close> and
    confl_nil: \<open>get_conflict_wl S = None\<close> and
    trail_in_NP: \<open>\<forall>L\<in>lits_of_l (get_trail_wl S). {#L#} \<in># get_unit_init_clss S\<close> and
    prop_NP: \<open>\<forall>L \<in> set (get_trail_wl S). \<exists>K. L = Propagated K 0\<close>
    unfolding S_def by (auto simp:
        twl_struct_invs_def twl_st_inv.simps cdcl\<^sub>W_restart_mset.cdcl\<^sub>W_all_struct_inv_def
        cdcl\<^sub>W_restart_mset.no_strange_atm_def cdcl\<^sub>W_restart_mset.cdcl\<^sub>W_M_level_inv_def
        cdcl\<^sub>W_restart_mset.distinct_cdcl\<^sub>W_state_def cdcl\<^sub>W_restart_mset.cdcl\<^sub>W_conflicting_def
        cdcl\<^sub>W_restart_mset.cdcl\<^sub>W_learned_clause_def cdcl\<^sub>W_restart_mset.no_smaller_propa_def
        past_invs.simps clauses_def additional_WS_invs_def twl_stgy_invs_def clause_to_update_def
        cdcl\<^sub>W_restart_mset_state cdcl\<^sub>W_restart_mset.cdcl\<^sub>W_stgy_invariant_def
        cdcl\<^sub>W_restart_mset.no_smaller_confl_def get_unit_learned_def)
  note HH = init_dt_init_dt_l_full[of CS S, unfolded N\<^sub>0_def[symmetric] N\<^sub>1_def[symmetric] clss_empty,
        OF dist length taut struct dec confl aff_invs learned stgy_invs watch clss _ learned_nil,
        unfolded empty_neutral trail_in_NP]
  have [simp]: \<open>mset `# mset (take ag (tl af)) + ai + (mset `# mset (drop (Suc ag) af)) =
     mset `# mset (tl af) + ai\<close> for ag af aj ai
    by (subst (2) append_take_drop_id[symmetric, of \<open>tl af\<close> ag], subst mset_append)
      (auto simp: drop_Suc)
  have [simp]: \<open>twl_array_code.N\<^sub>1 N\<^sub>0 = N\<^sub>1\<close>
    by (auto simp: N\<^sub>1_def twl_array_code.N\<^sub>1_def  twl_array_code.N\<^sub>0''_def  twl_array_code.N\<^sub>0'_def)
      have [simp]: \<open>L \<notin> (\<lambda>x. - literal_of_nat x) ` set N\<^sub>0 \<longleftrightarrow> -L \<notin> literal_of_nat ` set N\<^sub>0\<close> for L
        by (cases L) (auto simp: split: if_splits)
  have H: \<open>xa \<in> set N\<^sub>0 \<Longrightarrow>
          Pos (atm_of (literal_of_nat xa)) \<in> literal_of_nat ` set N\<^sub>0 \<or>
          Neg (atm_of (literal_of_nat xa)) \<in> literal_of_nat ` set N\<^sub>0\<close> for xa
    by (cases \<open>literal_of_nat xa\<close>) (auto split: if_splits)
  have \<open>set_mset N\<^sub>1 \<subseteq> set_mset (lits_of_atms_of_m N\<^sub>1)\<close>
    by (fastforce simp: lits_of_atms_of_m_def N\<^sub>1_def image_image uminus_lit_swap)
  then have [simp]: \<open>set_mset (lits_of_atms_of_m N\<^sub>1) = set_mset N\<^sub>1\<close>
    by (fastforce simp: lits_of_atms_of_m_def N\<^sub>1_def image_image uminus_lit_swap
        simp del: literal_of_nat.simps dest: H)
  show ?thesis
    apply (rule order.trans)
     apply (rule HH)
    by (clarsimp_all simp: correct_watching.simps twl_array_code.is_N\<^sub>1_def clauses_def
        mset_take_mset_drop_mset' get_unit_learned_def confl_nil trail_in_NP prop_NP)
qed

definition init_state :: \<open>nat clauses \<Rightarrow> nat cdcl\<^sub>W_restart_mset\<close> where
  \<open>init_state N = (([]:: (nat, nat clause) ann_lits), (N :: nat clauses), ({#}::nat clauses),
      (None :: nat clause option))\<close>

definition empty_watched :: \<open>nat list \<Rightarrow> nat literal \<Rightarrow> nat list\<close> where
  \<open>empty_watched _ = (\<lambda>_. [])\<close>

locale locale_nat_list =
  fixes N :: \<open>nat list\<close>

definition (in locale_nat_list) init_state_wl :: \<open>nat \<Rightarrow> nat twl_st_wl\<close> where
  \<open>init_state_wl _ = ([], [[]], 0, None, {#}, {#}, {#}, empty_watched N)\<close>

text \<open>to get a full SAT:
  \<^item> either we fully apply \<^term>\<open>cdcl\<^sub>W_restart_mset.cdcl\<^sub>W_stgy\<close>
  \<^item> or we can stop early.
\<close>
definition SAT :: \<open>nat clauses \<Rightarrow> nat cdcl\<^sub>W_restart_mset nres\<close> where
  \<open>SAT CS = do{
    let T = init_state CS;
    SPEC (\<lambda>U. full cdcl\<^sub>W_restart_mset.cdcl\<^sub>W_stgy T U \<or>
         (cdcl\<^sub>W_restart_mset.clauses U = CS \<and> conflicting U \<noteq> None \<and> count_decided (trail U) = 0))
  }\<close>

definition SAT_wl :: \<open>nat clauses_l \<Rightarrow> nat twl_st_wl nres\<close> where
  \<open>SAT_wl CS = do{
    let n = length CS;
    let N\<^sub>0 = map nat_of_lit (extract_lits_clss CS []);
    let S = locale_nat_list.init_state_wl N\<^sub>0 n;
    T \<leftarrow> init_dt_wl N\<^sub>0 CS S;
    if get_conflict_wl T = None
    then twl_array_code.cdcl_twl_stgy_prog_wl_D N\<^sub>0 T
    else RETURN T
  }\<close>

definition map_nat_of_lit where
  \<open>map_nat_of_lit = map nat_of_lit\<close>

definition SAT_wl' :: \<open>nat clauses_l \<Rightarrow> bool nres\<close> where
  \<open>SAT_wl' CS = do{
    let n = length CS;
    let N\<^sub>0 = map_nat_of_lit (extract_lits_clss CS []);
    let S = locale_nat_list.init_state_wl N\<^sub>0 n;
    T \<leftarrow> init_dt_wl N\<^sub>0 CS S;
    if get_conflict_wl T = None
    then do {
       U \<leftarrow> twl_array_code.cdcl_twl_stgy_prog_wl_D N\<^sub>0 T;
       RETURN (get_conflict_wl U = None)}
    else RETURN False
  }\<close>

lemma list_assn_map_list_assn: \<open>list_assn g (map f x) xi = list_assn (\<lambda>a c. g (f a) c) x xi\<close>
  apply (induction x arbitrary: xi)
  subgoal by auto
  subgoal for a x xi
    by (cases xi) auto
  done

text \<open>TODO Move\<close>
lemma lit_of_natP_nat_of_lit_iff: \<open>lit_of_natP c a \<longleftrightarrow> c = nat_of_lit a\<close>
  by (cases a) (auto simp: lit_of_natP_def)

lemma id_map_nat_of_lit[sepref_fr_rules]:
  \<open>(return o id, RETURN o map_nat_of_lit) \<in> (list_assn nat_lit_assn)\<^sup>d \<rightarrow>\<^sub>a (list_assn nat_assn)\<close>
  by sepref_to_hoare (sep_auto simp: map_nat_of_lit_def list_assn_map_list_assn p2rel_def lit_of_natP_nat_of_lit_iff)

definition (in locale_nat_list) init_state_wl_D :: \<open>nat \<Rightarrow> twl_st_wll Heap\<close> where
  \<open>init_state_wl_D l_Ns = do {
     let n = Suc (Suc (fold max N 0));
     N \<leftarrow> arrayO_raa_empty_sz l_Ns;
     e \<leftarrow> Array.new 0 0;
     N \<leftarrow> arrayO_raa_append N e;
     (WS) \<leftarrow> arrayO_ara_empty_sz_code n;
     return ([], N, 0, None, [], [], [], WS)
  }\<close>

lemma fold_cons_replicate: \<open>fold (\<lambda>_ xs. a # xs) [0..<n] xs = replicate n a @ xs\<close>
  by (induction n) auto

lemma arrayO_ara_empty_sz_code_rule:
   \<open><emp> arrayO_ara_empty_sz_code m
   <\<lambda>r. arrayO (arl_assn R) (replicate m []) r * true>\<close>
proof -
  have H: \<open>(\<exists>\<^sub>Ara. hn_val nat_rel n m * true * arrayO (arl_assn R) ra r * \<up> (ra = replicate n [])) =
     (hn_val nat_rel n m * true * arrayO (arl_assn R) (replicate n []) r)\<close> for r n m
    by (simp add: ex_assn_up_eq2)
  have \<open><hn_val nat_rel n m> arrayO_ara_empty_sz_code m
     <\<lambda>r. \<exists>\<^sub>Ara. hn_val nat_rel n m * arrayO (arl_assn R) ra r * true * \<up> (ra = replicate n [])>\<close>
    for n m :: nat
    by (rule imp_correctI[OF arrayO_ara_empty_sz_code.refine[to_hnr], simplified])
    (auto simp: arrayO_ara_empty_sz_def fold_cons_replicate)
  then show ?thesis
    by (simp add: ex_assn_up_eq2 hn_ctxt_def pure_def)
qed

lemma arrayO_ara_empty_sz_code_empty_watched:
  \<open>(uncurry0 (arrayO_ara_empty_sz_code (Suc (Suc (fold max N (0::nat))))),
     uncurry0 (RETURN (empty_watched N))) \<in>
   unit_assn\<^sup>k \<rightarrow>\<^sub>a twl_array_code.array_watched_assn N\<close>
proof -
  define n where \<open>n = Suc (Suc (fold max N 0))\<close>
  have n_def_alt: \<open>n = Max (set N) + 2\<close> if \<open>xa \<in> set N\<close> for xa
    unfolding n_def
    using that by (cases N, auto simp: Max.set_eq_fold list.set(2)[symmetric] simp del: list.set(2))

  have [simp]:
    \<open>xa \<in> set N \<Longrightarrow> xa < n\<close>
    \<open>xa \<in> set N \<Longrightarrow> Suc xa < n\<close>
    \<open>xa \<in> set N \<Longrightarrow> xa - Suc 0 < n\<close>
    for xa
    unfolding n_def_alt using Max_ge[of \<open>set N\<close> xa]
    by (simp_all del: Max_ge)

  have 1: \<open>(uncurry0 (RETURN (arrayO_ara_empty_sz n)),
     uncurry0  (RETURN (empty_watched N)))\<in>
   unit_rel \<rightarrow>\<^sub>f \<langle>\<langle>Id\<rangle>map_fun_rel (twl_array_code.D\<^sub>0 N)\<rangle> nres_rel\<close>
    by (intro nres_relI frefI)
       (auto simp: map_fun_rel_def arrayO_ara_empty_sz_def fold_cons_replicate
        twl_array_code.N\<^sub>1_def twl_array_code.N\<^sub>0'_def twl_array_code.N\<^sub>0''_def empty_watched_def
        simp del: replicate.simps)
  have 0: \<open>(uncurry0 (arrayO_ara_empty_sz_code n), uncurry0 (RETURN (arrayO_ara_empty_sz n))) \<in>
    unit_assn\<^sup>k \<rightarrow>\<^sub>a arrayO (arl_assn R)\<close> for R
    supply arrayO_ara_empty_sz_code_rule[sep_heap_rules]
    by sepref_to_hoare (sep_auto simp: arrayO_ara_empty_sz_def fold_cons_replicate)

  show ?thesis
    using 0[FCOMP 1] unfolding n_def[symmetric] PR_CONST_def .
qed

context locale_nat_list
begin
sepref_register init_state_wl
lemma init_state_wl_D_init_state_wl:
  \<open>(init_state_wl_D, RETURN o (PR_CONST init_state_wl)) \<in>
     nat_assn\<^sup>k \<rightarrow>\<^sub>a twl_array_code.twl_st_l_assn N\<close>
proof -
  have [simp]: \<open>clauses_l_assn {#} [] = emp\<close>
    by (auto simp: list_mset_assn_def list_mset_rel_def mset_rel_def
        br_def p2rel_def rel2p_def Collect_eq_comp rel_mset_def
        pure_def)
  have [simp]: \<open>clause_l_assn {#} [] = emp\<close>
    by (auto simp: list_mset_assn_def list_mset_rel_def mset_rel_def
        br_def p2rel_def rel2p_def Collect_eq_comp rel_mset_def
        pure_def)
  have [simp]: \<open>nat_assn n n = emp\<close> for n
    by (simp add: pure_app_eq)
  have e: \<open>RETURN $ empty_watched N \<le> SPEC (op = (empty_watched N))\<close>
    by auto
  note imp_correctI[OF arrayO_ara_empty_sz_code_empty_watched[to_hnr] e, sep_heap_rules]

  have [sep_heap_rules]:
      \<open><arrayO_raa clause_ll_assn l a * x \<mapsto>\<^sub>a []> arrayO_raa_append a x <arrayO_raa clause_ll_assn (l @ [[]])>\<^sub>t\<close>
    for l a x
    using arrayO_raa_append_rule[of clause_ll_assn l a \<open>[]\<close> x, unfolded]
      apply (subst (asm)(2) array_assn_def)
    by (auto simp: hr_comp_def ex_assn_def is_array_def)

  show ?thesis
    supply arl_empty_sz_array_rule[sep_heap_rules del]
    supply arl_empty_sz_array_rule[of _ clause_ll_assn, sep_heap_rules]
    apply sepref_to_hoare
    by (sep_auto simp: init_state_wl_D_def init_state_wl_def twl_array_code.twl_st_l_assn_def)
qed

concrete_definition (in -) init_state_wl_D_code
  uses "locale_nat_list.init_state_wl_D_init_state_wl"
  is "(?f,_)\<in>_"

prepare_code_thms (in -) init_state_wl_D_code_def

lemmas init_state_wl_D_code_refine[sepref_fr_rules] = init_state_wl_D_code.refine
end

sepref_register locale_nat_list.init_state_wl
lemmas init_state_wl_D_code_refine = locale_nat_list.init_state_wl_D_code_refine
term init_state_wl_D_code

lemma init_state_wl_D_code[unfolded twl_array_code.twl_st_l_assn_def, sepref_fr_rules]:
  \<open>(uncurry init_state_wl_D_code, uncurry (RETURN oo (locale_nat_list.init_state_wl)))
  \<in> [\<lambda>(N', _). N = N']\<^sub>a(list_assn nat_assn)\<^sup>k *\<^sub>a nat_assn\<^sup>k \<rightarrow> twl_array_code.twl_st_l_assn N\<close>
proof -
  have e: \<open>RETURN $ (locale_nat_list.init_state_wl N $ x) \<le> SPEC (op = (locale_nat_list.init_state_wl N x))\<close>
    for x
    by auto
  have [simp]: \<open>hn_val nat_rel x1 x1 = emp\<close> for x1
    by (simp add: hn_ctxt_def pure_def)
  have 1: \<open>(\<lambda>a c. \<up> (c = a)) = id_assn\<close>
    by (auto simp: pure_def)
  have [simp]: \<open>list_assn (\<lambda>a c. \<up> (c = a)) a ai = \<up> (a = ai)\<close> for a ai
    unfolding list_assn_pure_conv 1 by (auto simp: pure_def)
  have [sep_heap_rules]: \<open><emp> init_state_wl_D_code N x1
  <\<lambda>r. \<exists>\<^sub>Ara. twl_array_code.twl_st_l_assn N ra r *
               true *
               \<up> (locale_nat_list.init_state_wl N x1 = ra)>\<close> for x1 :: nat
    using imp_correctI[OF init_state_wl_D_code_refine[to_hnr, of x1 x1 N, unfolded PR_CONST_def] e,
        sep_heap_rules, simplified]
    by simp
  show ?thesis
    by sepref_to_hoare sep_auto
qed

sepref_definition SAT_wl_code
  is \<open>SAT_wl'\<close>
  :: \<open>(list_assn (list_assn nat_lit_assn))\<^sup>d \<rightarrow>\<^sub>a bool_assn\<close>
  unfolding SAT_wl'_def HOL_list.fold_custom_empty extract_lits_cls'_extract_lits_cls[symmetric]
    PR_CONST_def
  supply [[goals_limit = 1]]
  apply sepref_dbg_keep
  apply sepref_dbg_trans_keep
  -- \<open>Translation stops at the \<open>set\<close> operation\<close>
            apply sepref_dbg_trans_step_keep
            apply (rule_tac psi=\<open>xd = xd\<close> in asm_rl, rule refl)
  apply sepref_dbg_trans_keep
              apply sepref_dbg_trans_keep
    --\<open>code for init_dt_wl\<close>
  oops

definition f_conv :: \<open>(nat twl_st_wl \<times> nat cdcl\<^sub>W_restart_mset)set\<close> where
  \<open>f_conv = {(S', S). S = convert_to_state (twl_st_of_wl None S')}\<close>

lemma list_all2_eq_map_iff: \<open>list_all2 (\<lambda>x y. y = f x) xs ys \<longleftrightarrow> ys = map f xs\<close>
  apply (induction xs arbitrary: ys)
  subgoal by auto
  subgoal for a xs ys
    by (cases ys) auto
  done

lemma SPEC_add_information: \<open>P \<Longrightarrow> A \<le> SPEC Q \<Longrightarrow> A \<le> SPEC(\<lambda>x. Q x \<and> P)\<close>
  by auto

lemma cdcl_twl_stgy_prog_wl_spec_final2:
  shows
    \<open>(SAT_wl, SAT) \<in> [\<lambda>CS. (\<forall>C \<in># CS. distinct_mset C) \<and> (\<forall>C \<in># CS. size C \<ge> 1) \<and>
        (\<forall>C \<in># CS. \<not>tautology C)]\<^sub>f
     (list_mset_rel O \<langle>list_mset_rel\<rangle> mset_rel) \<rightarrow> \<langle>f_conv\<rangle>nres_rel\<close>
proof -
  have in_list_mset_rel: \<open>(CS', y) \<in> list_mset_rel \<longleftrightarrow> y = mset CS'\<close> for CS' y
    by (auto simp: list_mset_rel_def br_def)
  have in_list_mset_rel_mset_rel:
    \<open>(mset CS', CS) \<in> \<langle>list_mset_rel\<rangle>mset_rel \<longleftrightarrow> CS = mset `# mset CS'\<close> for CS CS'
    by (auto simp: list_mset_rel_def br_def mset_rel_def p2rel_def rel_mset_def
        rel2p_def[abs_def] list_all2_eq_map_iff)
  have [simp]: \<open>mset `# mset (take ag (tl af)) + ai + (mset `# mset (drop (Suc ag) af)) =
     mset `# mset (tl af) + ai\<close> for ag af aj ai
    by (subst (2) append_take_drop_id[symmetric, of \<open>tl af\<close> ag], subst mset_append)
      (auto simp: drop_Suc)
  show ?thesis
    unfolding SAT_wl_def SAT_def locale_nat_list.init_state_wl_def empty_watched_def
  apply (intro frefI nres_relI)
    apply (refine_vcg bind_refine_spec init_dt_init_dt_l)
       defer
    subgoal by (auto simp: in_list_mset_rel in_list_mset_rel_mset_rel)
    subgoal by (auto simp: in_list_mset_rel in_list_mset_rel_mset_rel)
    subgoal by (auto simp: in_list_mset_rel in_list_mset_rel_mset_rel)
    subgoal premises p for CS' CS S\<^sub>0
    proof (cases \<open>get_conflict_wl S\<^sub>0 = None\<close>)
      case False
      then have confl: \<open>get_conflict_wl S\<^sub>0 = None \<longleftrightarrow> False\<close>
        by auto
      have CS: \<open>CS = mset `# mset CS'\<close>
        using p by (auto simp: in_list_mset_rel in_list_mset_rel_mset_rel)
      show ?thesis
        unfolding confl if_False
        apply (rule RETURN_SPEC_refine)
        apply (rule exI[of _ \<open>convert_to_state (twl_st_of None (st_l_of_wl None S\<^sub>0))\<close>])
        apply (intro conjI)
       subgoal using p confl by (cases S\<^sub>0, clarsimp_all simp: f_conv_def mset_take_mset_drop_mset'
            clauses_def get_unit_learned_def in_list_mset_rel in_list_mset_rel_mset_rel)
       subgoal
         apply (rule disjI2)
          using p False by (cases S\<^sub>0) (clarsimp simp: CS init_state_def full_def cdcl\<^sub>W_restart_mset_state)
        done
    next
      case True
      then have confl: \<open>get_conflict_wl S\<^sub>0 = None \<longleftrightarrow> True\<close>
        by auto
      obtain M N NP Q W where
        S\<^sub>0: \<open>S\<^sub>0 = (M, N, length N - 1, None, NP, {#}, Q, W)\<close>
        using p True by (cases S\<^sub>0) (auto simp: clauses_def mset_take_mset_drop_mset' get_unit_learned_def)
      have N_NP: \<open>mset `# mset (tl N) + NP = mset `# mset CS'\<close>
        using p by (auto simp: clauses_def mset_take_mset_drop_mset' S\<^sub>0)
      have trail_in_NP: \<open>\<forall>L\<in>lits_of_l M. {#L#} \<in># NP\<close> and
        struct: \<open>twl_struct_invs (twl_st_of_wl None S\<^sub>0)\<close>
        using p unfolding S\<^sub>0 by (auto simp: get_unit_init_clss_def)
      have n_d: \<open>no_dup M\<close>
        using struct by (auto simp: twl_struct_invs_def S\<^sub>0
            cdcl\<^sub>W_restart_mset.cdcl\<^sub>W_all_struct_inv_def cdcl\<^sub>W_restart_mset.cdcl\<^sub>W_conflicting_def
            cdcl\<^sub>W_restart_mset_state cdcl\<^sub>W_restart_mset.cdcl\<^sub>W_M_level_inv_def)
      have prop_M: \<open>\<forall>L\<in> set M. \<exists>K. L = Propagated K 0\<close>
        using p by (auto simp: S\<^sub>0)
      have CS: \<open>CS = mset `# mset CS'\<close>
        using p by (auto simp: in_list_mset_rel in_list_mset_rel_mset_rel)
      have 0: \<open>cdcl\<^sub>W_restart_mset.cdcl\<^sub>W_stgy\<^sup>*\<^sup>* ([], CS, {#}, None)
         (convert_lits_l N M, mset `# mset CS', {#}, None)\<close>
        using trail_in_NP prop_M n_d
        apply (induction M)
        subgoal by (auto simp: CS)
        subgoal for L M
          apply simp
          apply (rule rtranclp.rtrancl_into_rtrancl)
           apply assumption
          apply (rule cdcl\<^sub>W_restart_mset.cdcl\<^sub>W_stgy.propagate')
            apply (cases L)
           apply (auto simp: cdcl\<^sub>W_restart_mset.propagate.simps cdcl\<^sub>W_restart_mset_state clauses_def CS
              N_NP[symmetric])
          done
        done
      then have 1: \<open>cdcl\<^sub>W_restart_mset.cdcl\<^sub>W_stgy\<^sup>*\<^sup>* (init_state CS)
         (convert_to_state (twl_st_of None (st_l_of_wl None S\<^sub>0)))\<close>
        using 0 by (auto simp: S\<^sub>0 CS mset_take_mset_drop_mset' N_NP init_state_def)
      have 2: \<open>twl_array_code.cdcl_twl_stgy_prog_wl_D (map nat_of_lit (extract_lits_clss CS' [])) S\<^sub>0
         \<le> SPEC (\<lambda>T. full cdcl\<^sub>W_restart_mset.cdcl\<^sub>W_stgy
                       (convert_to_state (twl_st_of None (st_l_of_wl None S\<^sub>0)))
                       (convert_to_state (twl_st_of None (st_l_of_wl None T))))\<close>
        using twl_array_code.cdcl_twl_stgy_prog_wl_spec_final2[of S\<^sub>0 \<open>(map nat_of_lit (extract_lits_clss CS' []))\<close>]
        using p 1 True by auto

      have \<open>twl_array_code.cdcl_twl_stgy_prog_wl_D (map nat_of_lit (extract_lits_clss CS' [])) S\<^sub>0
        \<le> \<Down> f_conv
        (SPEC (full cdcl\<^sub>W_restart_mset.cdcl\<^sub>W_stgy (CDCL_Two_Watched_Literals_List_Watched_Init_Code.init_state CS)))\<close>
        by (auto simp: f_conv_def conc_fun_RES rtranclp_fullI
            intro!: weaken_SPEC[OF SPEC_add_information[OF 1 2]])
      then show ?thesis
        unfolding confl if_True by (rule ref_two_step) auto
    qed
    done
qed

end