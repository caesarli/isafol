# Formalization of Bachmair and Ganzinger's "Resolution Theorem Proving" #

[This directory](https://bitbucket.org/isafol/isafol/src/master/Bachmair_Ganzinger/) contains an ongoing Isabelle formalization of Sections 3 and 4 of Bachmair and Ganzinger's "Resolution Theorem Proving" (Chapter 2 of Volume 1 of _The Handbook of Automated Reasoning_).

The repository version of Isabelle is necessary to process the theory files. For Isabelle2016 compatible version,  switch to the ``IJCAR2016`` branch (change with  ``git checkout IJCAR2016``). The [Archive of Formal Proofs](http://afp.sourceforge.net/) is also needed.

## Authors ##

* [Anders Schlichtkrull](mailto:anders shtrudel dtu.dk)
* [Jasmin Christian Blanchette](mailto:jasmin.blanchette shtrudel inria.fr)
* [Dmitriy Traytel](mailto:traytel shtrudel inf.ethz.ch)
