var searchData=
[
  ['set_5fresize',['set_resize',['../gratgen_8cpp.html#a6df775ef195063e1b7c68b54482ac63d',1,'gratgen.cpp']]],
  ['set_5ftrpos',['set_trpos',['../classitem__t.html#a865afbc8c6d5ee2e30c238294a13d89a',1,'item_t']]],
  ['shrink_5fto',['shrink_to',['../classClauseDB.html#a7de50f4b8dd784a1c188e5b5260bb839',1,'ClauseDB::shrink_to(pos_t pos)'],['../classClauseDB.html#a08c064b446cc7c28ba46555ef2aaf577',1,'ClauseDB::shrink_to(vector&lt; cdb_t &gt;::iterator end)']]],
  ['stat_5fbwd_5fchecking_5ftime',['stat_bwd_checking_time',['../gratgen_8cpp.html#a1f6dc051f512cbe1239cf3b180825f15',1,'gratgen.cpp']]],
  ['stat_5fchecking_5ftime',['stat_checking_time',['../gratgen_8cpp.html#acdca1941bed817cbc9ddde19faba4124',1,'gratgen.cpp']]],
  ['stat_5fdb_5fsize',['stat_db_size',['../gratgen_8cpp.html#afc0d617dcec2c33a046c8f72fdf56f87',1,'gratgen.cpp']]],
  ['stat_5fitemlist_5fsize',['stat_itemlist_size',['../gratgen_8cpp.html#af053d8518144182bf0ef4926640668d4',1,'gratgen.cpp']]],
  ['stat_5foverall_5ftime',['stat_overall_time',['../gratgen_8cpp.html#ae7d6214f1a5e9861eaff6fe696c3096b',1,'gratgen.cpp']]],
  ['stat_5foverall_5fvrf_5ftime',['stat_overall_vrf_time',['../gratgen_8cpp.html#ace1731796b6e993db226f4af903f949e',1,'gratgen.cpp']]],
  ['stat_5fparsing_5ftime',['stat_parsing_time',['../gratgen_8cpp.html#a734c1b80a5fe5bd42a16f38564c66a2c',1,'gratgen.cpp']]],
  ['stat_5fpivots_5fsize',['stat_pivots_size',['../gratgen_8cpp.html#a738431be8d845368cc4c3eca99dda070',1,'gratgen.cpp']]],
  ['stat_5fwriting_5ftime',['stat_writing_time',['../gratgen_8cpp.html#a7f9ef15a946d8432d3f538826917b719',1,'gratgen.cpp']]],
  ['synch_5fdata',['Synch_Data',['../classSynch__Data.html#a9baafb3b3f399cdb3dc914bd717b6e9b',1,'Synch_Data']]]
];
