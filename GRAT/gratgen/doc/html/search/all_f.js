var searchData=
[
  ['rat_5fcounts',['RAT_COUNTS',['../gratgen_8cpp.html#a8c2e74cbe5228c02be4d77db10cce705a1d6b76d1eeb5c7bcaec73252e49d9fca',1,'gratgen.cpp']]],
  ['rat_5flemma',['RAT_LEMMA',['../gratgen_8cpp.html#a8c2e74cbe5228c02be4d77db10cce705af84bb200054cebf49d839eb4ea1ff8fb',1,'gratgen.cpp']]],
  ['readd_5fclause',['readd_clause',['../classVerifier.html#a4a24b94116d990d1a9108747942fab67',1,'Verifier']]],
  ['reason',['reason',['../structtrail__item__t.html#a624c7feb8afeadbeda9f3b4e4b0dc585',1,'trail_item_t']]],
  ['rem_5fclause',['rem_clause',['../classVerifier.html#a920f17f69b934a34999e78206f674a3b',1,'Verifier']]],
  ['resize_5freset',['resize_reset',['../classlit__map.html#a7465c8a3bee32959434ebfe4a0fe49ed',1,'lit_map']]],
  ['rollback',['rollback',['../classVerifier.html#ae0393e5f6d52f036ece9160213381a41',1,'Verifier']]],
  ['rup_5flemma',['RUP_LEMMA',['../gratgen_8cpp.html#a8c2e74cbe5228c02be4d77db10cce705aaea9233bb77c710c5ca9d18cdaa6453a',1,'gratgen.cpp']]]
];
